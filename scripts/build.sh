#!/usr/bin/env bash

k6_version="${K6_VERSION:-v0.46.0}"
xk6_path="${XK6_PATH:-./bin/xk6}"
xk6_with="${XK6_WITH:-gitlab.com/gitlab-org/opstrace/tools/xk6-client-tracing@latest}"
out_path="${XK6_OUT_PATH:-bin/k6}"

$xk6_path build "$k6_version" \
    --with $xk6_with \
    --replace cloud.google.com/go=cloud.google.com/go@v0.110.8 \
    --output $out_path