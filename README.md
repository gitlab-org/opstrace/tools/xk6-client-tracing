# xk6-client-tracing

This extension provides k6 with the required functionality required to load test distributed tracing backends, specifically OpenTelemetry and Jaeger.

[Read more about k6 extensions](https://k6.io/docs/extensions/).

[Read more about the k6 Go/js bridge](https://k6.io/docs/extensions/explanations/go-js-bridge/).

## Getting started  

To start using k6 with the extension, ensure you have the prerequisites:

- [Go toolchain](https://go101.org/article/go-toolchain.html)
- Git

See also `.tool-versions` for asdf support.

Then:

1. Build the custom k6 binary:
```shell
make
```

Once you've your new binary ready, you can run a local OTEL collector and Jaeger instance with:
```shell
docker compose up
```

Once that's done, you can run a test like:
```
./bin/k6 run examples/basic.js
```

And see your spans on the OTEL collector logs!

Open the Jaeger UI at http://localhost:16686/ and you should see your traces there too.

The example uses the OTLP gRPC exporter. If you want to use OTLP HTTP exporter, you can use these settings:

```javascript
const client = new tracing.Client({
    endpoint: "0.0.0.0:4318/v1/traces",
    exporter: "otlphttp",
    insecure: true,
});
```

## API

Import this module in your k6 script:

```javascript
import tracing from 'k6/x/tracing';
```

Make a tracing client:

```javascript
const client = new tracing.Client({
    endpoint: "http://0.0.0.0:4318/v1/traces",
    // exporter is optional - defaults to otlp
    exporter: "otlphttp",
    // headers can be a map of anything
    headers: {
        'Authorization': 'Bearer potato',
    },
    // insecure is optional - defaults to false
    // if true, it will use a non-TLS connection
    insecure: true,
    // insecure_skip_verify is optional - defaults to false
    // if true, it will skip TLS validation
    insecure_skip_verify: true,
    // loglevel is optional - defaults to info
    // maps to zapcore.Level
    loglevel: "debug"
});
```

Send one or more traces:

```javascript
client.push([{
    // TraceID to use for this trace in hex format
    id: traceUUID,
    // random_service_name is optional - defaults to false
    // if true, it will add random characters into service names
    random_service_name: true,
    // start_time is optional
    // if set this is used for each span start time
    // otherwise the current time is used
    start_time: new Date(),
    spans: {
        // the number of spans to generate for the trace ID
        count: 1,
        // generated attributes collection in bytes
        size: 50,
        // random_name is optional - defaults to false
        // adds a random string to the end of selected span name
        random_name: true,
        // fixed attributes to send with each span
        fixed_attrs: {
            "test": "foo"
        }
    }
}])
```

`client.pushDebug([...])` will also log some info to the console.

You can use `tracing.generateRandomTraceID()` to create a valid random trace ID.

`client.exportMetrics()` will export the metrics collected by OTEL client to k6. This can impact on sending speed so is optional.

At the end of your k6 script, do:

```javascript
export function teardown() {
    client.shutdown();
}
```

## License

While k6 is licensed under [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)
scripts using k6 and xk6 extensions can be licensed however we like as long as they are
not distributed as part of a product.

References:

- https://k6.io/blog/extending-k6-with-xk6/ - suggests extension authors use Apache 2.0.
- https://community.k6.io/t/k6-licensing-agplv3/395 - support discussion for licensing.
- https://github.com/grafana/xk6-amqp - official xk6 extension using the Apache 2.0 license.
- https://github.com/grafana/xk6-kubernetes - and another, as are all others.
