package xk6_client_tracing

import (
	"fmt"
	"net"
	"net/http"
	"net/http/httptrace"
	"strconv"
	"sync"

	"go.k6.io/k6/js/modules"
	"go.k6.io/k6/lib"
	"go.k6.io/k6/lib/netext"
	"go.k6.io/k6/lib/netext/httpext"
	"go.k6.io/k6/metrics"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/config/confighttp"
)

// transport roundtripper merges the otel roundtripper and the k6 transport/dialer.
// This merges both metrics from k6 and those inside otel and mixes default settings.
type transport struct {
	otelSettings      confighttp.HTTPClientSettings
	vu                modules.VU
	host              component.Host
	telemetrySettings component.TelemetrySettings

	once         sync.Once
	roundTripper http.RoundTripper
}

func newTransport(
	otelSettings confighttp.HTTPClientSettings,
	vu modules.VU,
	host component.Host,
	telemetrySettings component.TelemetrySettings,
) *transport {
	return &transport{
		otelSettings:      otelSettings,
		vu:                vu,
		host:              host,
		telemetrySettings: telemetrySettings,
	}
}

// RoundTrip implements the http.RoundTripper.
// We inject the k6 transport into the configured otel http client as a base transport.
// The http client is discarded in favour of the final configured transport from otel.
// This provides the k6 dialer metrics (data used) and settings by default.
// Then, use the k6 http tracer to gather http specific metrics around the request.
func (t *transport) RoundTrip(req *http.Request) (*http.Response, error) {
	// HACK - recreate otel client/transport with k6 transport injected
	t.once.Do(func() {
		// otel uses default transport as base
		http.DefaultTransport = t.vu.State().Transport
		c, err := t.otelSettings.ToClient(t.host, t.telemetrySettings)
		if err != nil {
			panic(fmt.Errorf("creating otel http client: %w", err))
		}
		t.roundTripper = c.Transport
	})

	ctx := req.Context()
	tracer := &httpext.Tracer{}
	reqWithTracer := req.WithContext(httptrace.WithClientTrace(ctx, tracer.Trace()))
	resp, err := t.roundTripper.RoundTrip(reqWithTracer)

	trail := tracer.Done()

	go collectHTTPMetrics(t.vu.State(), req, trail, resp, err)

	return resp, err
}

// Collect HTTP metrics after request.
// Handles potential closure of state samples in a recover.
// Unfortunately, neither the VU context or k6 "teardown" function will adequately
// fire before this samples channel is closed.
func collectHTTPMetrics(state *lib.State, req *http.Request, trail *httpext.Trail, resp *http.Response, httpErr error) {
	defer func() {
		// recover required as the samples channel is closed before this function is called.
		if r := recover(); r != nil {
			state.Logger.Debugf("Metrics collection failed for request: %s", r)
		}
	}()

	tags := state.Tags.GetCurrentValues().Clone()
	applyTags(state, &tags, req, trail, resp, httpErr)

	builtinMetrics := state.BuiltinMetrics
	trail.SaveSamples(builtinMetrics, &tags)

	failed := float64(0)
	if httpErr != nil || resp.StatusCode > 299 {
		failed = 1
	}
	trail.Samples = append(trail.Samples,
		metrics.Sample{
			TimeSeries: metrics.TimeSeries{
				Metric: builtinMetrics.HTTPReqFailed,
				Tags:   tags.Tags,
			},
			Time:  trail.EndTime,
			Value: failed,
		},
	)

	state.Samples <- trail
}

// tags from the request/response and trail for each sampled http metric
func applyTags(state *lib.State, tags *metrics.TagsAndMeta, req *http.Request,
	trail *httpext.Trail,
	resp *http.Response, respErr error) {
	enabledTags := state.Options.SystemTags
	url, err := httpext.NewURL(req.RequestURI, "")
	if err != nil {
		panic(err)
	}
	cleanURL := url.Clean()

	// After k6 v0.41.0, the `name` and `url` tags have the exact same values:
	nameTagValue, nameTagManuallySet := tags.Tags.Get(metrics.TagName.String())
	if !nameTagManuallySet {
		// If the user *didn't* manually set a `name` tag value and didn't use
		// the http.url template literal helper to have k6 automatically set
		// it (see `lib/netext/httpext.MakeRequest()`), we will use the cleaned
		// URL value as the value of both `name` and `url` tags.
		tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagName, cleanURL)
		tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagURL, cleanURL)
	} else {
		// However, if the user set the `name` tag value somehow, we will use
		// whatever they set as the value of the `url` tags too, to prevent
		// high-cardinality values in the indexed tags.
		tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagURL, nameTagValue)
	}

	tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagMethod, req.Method)

	if respErr != nil {
		tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagError, respErr.Error())
		tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagStatus, "0")
	} else {
		tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagStatus, strconv.Itoa(resp.StatusCode))
		tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagProto, resp.Proto)

		if resp.TLS != nil {
			tlsInfo, oscp := netext.ParseTLSConnState(resp.TLS)
			tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagTLSVersion, tlsInfo.Version)
			tags.SetSystemTagOrMetaIfEnabled(enabledTags, metrics.TagOCSPStatus, oscp.Status)
		}
	}

	if enabledTags.Has(metrics.TagIP) && trail.ConnRemoteAddr != nil {
		if ip, _, err := net.SplitHostPort(trail.ConnRemoteAddr.String()); err == nil {
			tags.SetSystemTagOrMeta(metrics.TagIP, ip)
		}
	}
}
