package xk6_client_tracing

import (
	crand "crypto/rand"
	"encoding/hex"
	"fmt"
	"math/rand"
	"time"
	"unsafe"

	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/ptrace"
)

func generateResource(t TraceEntry, dest ptrace.ResourceSpans) {
	serviceName := pickServiceName()
	if t.RandomServiceName {
		serviceName += "." + randString(5)
	}
	dest.Resource().Attributes().PutBool("k6", true)
	// service.name maps to the "service" resource tag in OTEL
	dest.Resource().Attributes().PutStr("service.name", serviceName)

	scopeSpans := dest.ScopeSpans()
	scopeSpans.EnsureCapacity(1)
	ils := scopeSpans.AppendEmpty()
	ils.Scope().SetName("k6")

	// Spans
	sps := ils.Spans()
	sps.EnsureCapacity(t.Spans.Count)

	for e := 0; e < t.Spans.Count; e++ {
		generateSpan(t, sps.AppendEmpty())
		// add a parent ID
		if e > 0 {
			sps.At(e).SetParentSpanID(sps.At(e - 1).SpanID())
		}
	}
}

func generateSpan(t TraceEntry, dest ptrace.Span) {
	duration := time.Duration(rand.Intn(500)+10) * time.Millisecond

	var startTime, endTime time.Time

	// generate spans starting just before now if StartTime is not set
	// otherwise use the specific StartTime with added duration
	if t.StartTime.IsZero() {
		endTime = time.Now().Round(time.Second)
		startTime = endTime.Add(-duration)
	} else {
		startTime = t.StartTime
		endTime = startTime.Add(duration)
	}

	var b [16]byte
	traceID, _ := hex.DecodeString(t.ID)
	copy(b[:], traceID)

	spanName := pickOperationName()
	if t.Spans.RandomName {
		spanName += "." + randString(5)
	}

	span := ptrace.NewSpan()
	span.SetTraceID(pcommon.TraceID(b))
	span.SetSpanID(randSpanID())
	span.SetName(spanName)
	span.SetKind(ptrace.SpanKindClient)
	span.SetStartTimestamp(pcommon.NewTimestampFromTime(startTime))
	span.SetEndTimestamp(pcommon.NewTimestampFromTime(endTime))

	event := span.Events().AppendEmpty()
	event.SetName(randStringWithk6Prefix(12))
	event.SetTimestamp(pcommon.NewTimestampFromTime(startTime))
	event.Attributes().PutStr(randStringWithk6Prefix(12), randStringWithk6Prefix(12))

	status := span.Status()
	status.SetCode(1)
	status.SetMessage("OK")

	attrs := pcommon.NewMap()

	if len(t.Spans.FixedAttrs) > 0 {
		constructSpanAttributes(t.Spans.FixedAttrs, attrs)
	}

	// Fill the span with some random data
	var size int64
	for {
		if size >= int64(t.Spans.Size) {
			break
		}

		rKey := randStringWithk6Prefix(rand.Intn(15))
		rVal := randStringWithk6Prefix(rand.Intn(15))
		attrs.PutStr(rKey, rVal)

		size += int64(unsafe.Sizeof(rKey)) + int64(unsafe.Sizeof(rVal))
	}

	attrs.CopyTo(span.Attributes())
	span.CopyTo(dest)
}

func randSpanID() pcommon.SpanID {
	var r [8]byte
	_, err := crand.Read(r[:])
	if err != nil {
		panic(err)
	}
	return pcommon.SpanID(r)
}

func randString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return "" + string(s)
}

func randStringWithk6Prefix(n int) string {
	return "k6." + randString(n)
}

func pickServiceName() string {
	serviceNames := []string{
		"redis",
		"mysql",
		"postgres",
		"memcached",
		"mongodb",
		"elasticsearch",
		"kafka",
		"rabbitmq",
		"order",
		"payment",
		"customer",
		"product",
		"inventory",
		"shipping",
		"billing",
		"notification",
		"analytics",
		"search",
		"recommendation",
		"recommendation-engine",
		"recommendation-service",
		"recommendation-service-api",
		"recommendation-service-impl",
		"recommendation-service-proxy"}
	return "k6." + serviceNames[rand.Intn(len(serviceNames))]
}

func pickOperationName() string {
	operationNames := []string{
		"get",
		"set",
		"delete",
		"create",
		"update",
		"delete",
		"list",
		"search",
		"add",
		"remove"}
	objectNames := []string{
		"customer",
		"product",
		"order",
		"payment",
		"shipment",
		"bill"}
	return "k6." + objectNames[rand.Intn(len(objectNames))] + "." + operationNames[rand.Intn(len(operationNames))]
}

func constructSpanAttributes(attributes map[string]interface{}, dst pcommon.Map) {
	attrs := pcommon.NewMap()
	for key, value := range attributes {
		if cast, ok := value.(int); ok {
			attrs.PutInt(key, int64(cast))
		} else if cast, ok := value.(int64); ok {
			attrs.PutInt(key, cast)
		} else {
			attrs.PutStr(key, fmt.Sprintf("%v", value))
		}
	}
	attrs.CopyTo(dst)
}
