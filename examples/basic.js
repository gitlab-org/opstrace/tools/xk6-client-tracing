import { check } from 'k6';
import tracing from 'k6/x/tracing';
import { randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import { SharedArray } from 'k6/data';

export const options = {
    // 10 virtual users = 10 collector clients
    vus: 10,
    duration: "30s",
};

const traceIDs = new SharedArray('traceIDs', function () {
    let toret = [];
    for (let i = 0; i < 10; i++) {
        toret.push(tracing.generateRandomTraceID());
    }
    return toret;
});

const client = new tracing.Client({
    endpoint: "http://0.0.0.0:4318/v1/traces",
    exporter: "otlphttp",
    headers: {
        'Authorization': 'Bearer potato',
    }
});

export default function () {
    let pushSizeTraces = randomIntBetween(2,3);
    let pushSizeSpans = 0;
    let t = [];
    for (let i = 0; i < pushSizeTraces; i++) {
        let c = randomIntBetween(5,10)
        pushSizeSpans += c;

        t.push({
            id: traceIDs[Math.floor(Math.random() * traceIDs.length)],
            random_service_name: false,
            spans: {
                count: c,
                size: randomIntBetween(300,1000),
                random_name: true,
                fixed_attrs: {
                    "test": "test",
                },
            }
        });
    }
    const err = client.push(t)
    check(err, {
        'push err is nil': (r) => r === null,
    });
    if (err !== null) {
        console.error(err)
    }
    console.log(`Pushed ${pushSizeSpans} spans from ${pushSizeTraces} different traces. Here is a random traceID: ${t[Math.floor(Math.random() * t.length)].id}`);

    client.exportMetrics();
}

export function teardown() {
    client.shutdown();
}