// k6 script to generate a big old trace

import { sleep } from 'k6';
import tracing from 'k6/x/tracing';

// how many spans do we want to generate for the trace?
const spans = 500;

export const options = {
    vus: 1,
    // just a one-shot
    iterations: 1
};

const client = new tracing.Client({
    endpoint:  __ENV.TRACES_ADDRESS,
    exporter: "otlphttp",
    headers: {
        "x-target-projectid": __ENV.PROJECT_ID
    }
});

export default function () {
    client.push([
        {
            id: tracing.generateRandomTraceID(),
            spans: {
                count: spans,
                size: 50,
                fixed_attrs: {
                    "test": "test",
                },
            }
        }
    ]);

    // give the exporter chance to do its thing
    sleep(5);

    client.exportMetrics();
}

export function teardown() {
    client.shutdown();
}