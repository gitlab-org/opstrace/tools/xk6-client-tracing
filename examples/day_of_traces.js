// k6 script to generate traces over the last 24 hours.

import exec from 'k6/execution';
import tracing from 'k6/x/tracing';
import { randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';

// how many hours of data do we want to generate?
const hours = 24;

export const options = {
    vus: 1,
    // generate a trace for every second of the last "n hours"
    iterations: hours * 3600
};

const client = new tracing.Client({
    endpoint:  __ENV.TRACES_ADDRESS,
    exporter: "otlphttp",
    headers: {
        "x-target-projectid": __ENV.PROJECT_ID
    }
});

export default function () {
    let start = new Date(exec.scenario.startTime);
    start.setHours(start.getHours() - hours);
    // add seconds based on current iteration number
    start.setSeconds(start.getSeconds() + exec.scenario.iterationInInstance);

    client.push([
        {
            id: tracing.generateRandomTraceID(),
            start_time: start,
            random_service_name: false,
            spans: {
                count: randomIntBetween(1,12),
                size: randomIntBetween(50,500),
                random_name: true,
                fixed_attrs: {
                    "test": "test",
                },
            }
        }
    ]);

    client.exportMetrics();
}

export function teardown() {
    client.shutdown();
}