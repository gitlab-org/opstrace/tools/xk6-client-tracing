package xk6_client_tracing

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/dop251/goja"
	"go.k6.io/k6/js/common"
	"go.k6.io/k6/js/modules"
	"go.k6.io/k6/metrics"
	"go.opencensus.io/metric/metricdata"
	"go.opencensus.io/metric/metricproducer"
	"go.opencensus.io/resource"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/component/componenttest"
	"go.opentelemetry.io/collector/config/configgrpc"
	"go.opentelemetry.io/collector/config/configopaque"
	"go.opentelemetry.io/collector/config/configtelemetry"
	"go.opentelemetry.io/collector/config/configtls"
	"go.opentelemetry.io/collector/exporter"
	"go.opentelemetry.io/collector/exporter/otlpexporter"
	"go.opentelemetry.io/collector/exporter/otlphttpexporter"
	"go.opentelemetry.io/collector/pdata/ptrace"
	"go.opentelemetry.io/otel/metric/noop"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func init() {
	modules.Register("k6/x/tracing", new(tracingClientModule))
}

// ClientTracing is the k6 module for tracing.
type ClientTracing struct {
	vu modules.VU
}

type tracingClientModule struct{}

var _ modules.Module = &tracingClientModule{}

func (r *tracingClientModule) NewModuleInstance(vu modules.VU) modules.Instance {
	return &ClientTracing{vu: vu}
}

func (r *ClientTracing) Exports() modules.Exports {
	return modules.Exports{
		Named: map[string]interface{}{
			"Client":                r.NewClient,
			"generateRandomTraceID": r.GenerateRandomTraceID,
		},
	}
}

type exporterName string

const (
	noExporter       exporterName = ""
	otlpExporter     exporterName = "otlp"
	otlpHTTPExporter exporterName = "otlphttp"
)

// Client is a tracing client exported to the k6 script.
type Client struct {
	exporter exporter.Traces
	cfg      *Config
	vu       modules.VU
	registry *metrics.Registry
}

// Config holds the configuration for the tracing client.
type Config struct {
	// OTLP exporter type.
	Exporter exporterName
	// OTLP exporter endpoint.
	Endpoint string
	// Insecure allows insecure connection to the collector.
	Insecure bool
	// InsecureSkipVerify allows TLS without a valid certificate.
	InsecureSkipVerify bool
	// Headers to be added to the request.
	Headers map[string]configopaque.String
	// LogLevel sets the log level of the exporter.
	Loglevel string
}

// TraceEntry holds the config to generate a trace.
type TraceEntry struct {
	ID string `json:"id"`
	// StartTime is an optional start time for the trace.
	StartTime         time.Time  `json:"start_time"`
	RandomServiceName bool       `json:"random_service_name"`
	Spans             SpansEntry `json:"spans"`
}

// SpansEntry holds the config to generate spans.
type SpansEntry struct {
	Count      int                    `json:"count"`
	Size       int                    `json:"size"`
	RandomName bool                   `json:"random_name"`
	FixedAttrs map[string]interface{} `json:"fixed_attrs"`
}

// NewClient creates a new tracing client from the given config.
func (c *ClientTracing) NewClient(g goja.ConstructorCall) *goja.Object {
	log := c.vu.InitEnv().Logger
	var cfg Config
	rt := c.vu.Runtime()
	err := rt.ExportTo(g.Argument(0), &cfg)
	if err != nil {
		common.Throw(rt, fmt.Errorf("Client constructor expects first argument to be Config"))
	}

	log.Debugf("Tracing client config: %+v", cfg)

	if cfg.Endpoint == "" {
		cfg.Endpoint = "0.0.0.0:4317"
	}

	host := componenttest.NewNopHost()
	exporter, err := c.buildExporter(&cfg, host)
	if err != nil {
		log.Fatal(err)
	}

	if err := exporter.Start(context.Background(), host); err != nil {
		log.Fatal(err)
	}

	if err != nil {
		log.Fatal(fmt.Errorf("failed to create exporter: %v", err))
	}

	return rt.ToValue(&Client{
		exporter: exporter,
		cfg:      &cfg,
		vu:       c.vu,
		registry: c.vu.InitEnv().Registry,
	}).ToObject(rt)
}

// GenerateRandomTraceID generates a random trace ID encoded as a hex string.
func (c *ClientTracing) GenerateRandomTraceID() string {
	bytes := make([]byte, 16)
	if _, err := rand.Read(bytes); err != nil {
		return ""
	}
	return hex.EncodeToString(bytes)
}

func (c *ClientTracing) buildExporter(cfg *Config, host component.Host) (exporter.Traces, error) {
	var (
		factory     exporter.Factory
		exporterCfg component.Config
	)

	telemetrySettings := component.TelemetrySettings{
		Logger: zap.New(zapcore.NewCore(
			zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig()), zapcore.AddSync(os.Stdout), logLevel(cfg.Loglevel))),
		TracerProvider: trace.NewNoopTracerProvider(),
		MeterProvider:  noop.NewMeterProvider(),
		MetricsLevel:   configtelemetry.LevelDetailed,
	}

	tls := configtls.TLSClientSetting{
		Insecure:           cfg.Insecure,
		InsecureSkipVerify: cfg.InsecureSkipVerify,
	}

	switch cfg.Exporter {
	case noExporter, otlpExporter:
		factory = otlpexporter.NewFactory()
		exporterCfg = factory.CreateDefaultConfig()
		exporterCfg.(*otlpexporter.Config).GRPCClientSettings = configgrpc.GRPCClientSettings{
			Endpoint:   cfg.Endpoint,
			TLSSetting: tls,
			Headers:    cfg.Headers,
		}
	case otlpHTTPExporter:
		factory = otlphttpexporter.NewFactory()
		exporterCfg = factory.CreateDefaultConfig()
		conf := exporterCfg.(*otlphttpexporter.Config)
		conf.TracesEndpoint = cfg.Endpoint
		httpConf := conf.HTTPClientSettings
		httpConf.Headers = cfg.Headers
		httpConf.TLSSetting = tls
		transport := newTransport(httpConf, c.vu, host, telemetrySettings)
		// override transport with our k6/otel hyrbrid transport
		httpConf.CustomRoundTripper = func(next http.RoundTripper) (http.RoundTripper, error) {
			return transport, nil
		}
		conf.HTTPClientSettings = httpConf
	default:
		return nil, fmt.Errorf("failed to init exporter: unknown exporter type %s", cfg.Exporter)
	}

	return factory.CreateTracesExporter(
		context.Background(),
		exporter.CreateSettings{
			TelemetrySettings: telemetrySettings,
			BuildInfo:         component.NewDefaultBuildInfo(),
		},
		exporterCfg,
	)
}

// Push pushes a trace entry to the collector.
// Note - depending on the collector config this may just push to a queue and not actually send the data.
func (c *Client) Push(te []TraceEntry) *goja.Object {
	traceData := ptrace.NewTraces()

	rss := traceData.ResourceSpans()
	rss.EnsureCapacity(len(te))

	for _, t := range te {
		generateResource(t, rss.AppendEmpty())
	}

	return c.toError(c.exporter.ConsumeTraces(context.Background(), traceData))
}

// PushDebug pushes a trace entry to the collector and prints a debug message.
func (c *Client) PushDebug(te []TraceEntry) *goja.Object {
	for _, t := range te {
		c.vu.State().Logger.Info("Pushing traceID=", t.ID, " spans=", t.Spans.Count, " size=", t.Spans.Size)
	}
	return c.Push(te)
}

// ExportMetrics converts otel collector metrics to k6 metrics
func (c *Client) ExportMetrics() {
	samples := make(metrics.Samples, 0)
	state := c.vu.State()
	log := state.Logger

	m := metricproducer.GlobalManager()
	ps := m.GetAll()
	for _, p := range ps {
		ms := p.Read()
		for _, m := range ms {
			var metricType metrics.MetricType
			switch m.Descriptor.Type {
			case metricdata.TypeGaugeInt64, metricdata.TypeGaugeFloat64:
				metricType = metrics.Trend
			case metricdata.TypeCumulativeInt64, metricdata.TypeCumulativeFloat64:
				metricType = metrics.Counter
			default:
				log.Fatalf("unsupported otel metric type: %s", m.Descriptor.Type)
			}
			metric := c.registry.MustNewMetric(m.Descriptor.Name, metricType)

			resTags := map[string]string{}
			if m.Resource != nil {
				resTags["resource_type"] = m.Resource.Type
				resTags["resource_labels"] = resource.EncodeLabels(m.Resource.Labels)
			}

			tags := c.registry.RootTagSet()

			for _, ts := range m.TimeSeries {
				sts := map[string]string{}
				for i, k := range m.Descriptor.LabelKeys {
					lv := ts.LabelValues[i]
					if lv.Present {
						sts[k.Key] = lv.Value
					}
				}
				sampleTags := tags.WithTagsFromMap(sts)

				for _, p := range ts.Points {
					var val float64
					switch i := p.Value.(type) {
					case float64:
						val = i
					case int64:
						val = float64(i)
					// this should never happen, as we check above for metric types
					default:
						log.Fatalf("unsupported metric point type: %T", p.Value)
					}

					samples = append(samples, metrics.Sample{
						TimeSeries: metrics.TimeSeries{
							Metric: metric,
							Tags:   sampleTags,
						},
						Time:  p.Time,
						Value: val,
					})
				}
			}
		}
	}

	metrics.PushIfNotDone(c.vu.Context(), state.Samples, samples)
}

// Shutdown shuts down the exporter.
func (c *Client) Shutdown() *goja.Object {
	return c.toError(c.exporter.Shutdown(context.Background()))
}

// convert err to js object
// either null or a string from err.Error()
func (c *Client) toError(err error) *goja.Object {
	if err == nil {
		return nil
	}
	rt := c.vu.Runtime()
	return rt.ToValue(err.Error()).ToObject(rt)
}

func logLevel(l string) zapcore.Level {
	switch l {
	case "debug":
		return zapcore.DebugLevel
	case "info":
		return zapcore.InfoLevel
	case "warn":
		return zapcore.WarnLevel
	case "error":
		return zapcore.ErrorLevel
	default:
		return zapcore.InfoLevel
	}
}
