.PHONY: all
all: build

## Location to install dependencies to
LOCALBIN ?= $(shell pwd)/bin
$(LOCALBIN):
	mkdir -p $(LOCALBIN)

export XK6_PATH ?= $(LOCALBIN)/xk6 # xk6 required to build extension
K6 ?= $(LOCALBIN)/k6 # custom built k6 with extension

.PHONY: xk6
xk6: $(XK6_PATH) # Install XK6 for building k6 extensions
$(XK6_PATH): | $(LOCALBIN)
	GOBIN=$(LOCALBIN) go install go.k6.io/xk6/cmd/xk6@latest

# override default to build from local copy
export XK6_WITH=gitlab.com/gitlab-org/opstrace/tools/xk6-client-tracing=.

.PHONY: build
build: $(K6)
$(K6): xk6
	./scripts/build.sh

.PHONY: lint
lint:
	golangci-lint run --timeout 5m